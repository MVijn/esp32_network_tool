/***************************************************
  This is an example made by Adafruit and modifed by Olimex for MOD-LCD2.8RTP
  This demo was tested with Olimex MOD-LCD2.8RTP and ESP32-EVB and OLIMEXINO-2560.
  The boards were connected via UEXT connector and cable.
  
  Make sure to establish proper hardware connections with your board.
  The display requires SPI, the touschreen I2C. Refer to Board_Pinout.h.
  
  The original example is a GFX example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651

  Check out the link above for Adafruit's tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing the open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

 // In order to work you have to install Adafruit GFX Library
 // To do so go to:
 // Main menu --> Sketch --> Inlcude Librariy --> Manage Libraries...
 // In the search box filter "Adafruit GFX Library" and install it
 // Tested with version 1.2.3 of the library

#define ETH_CLK_MODE ETH_CLOCK_GPIO17_OUT
#define ETH_PHY_POWER 12

#include <ETH.h>
#include <ESP32Ping.h>

//time 
//#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

#include "Board_Pinout.h"
#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include "Wire.h"
#include "Adafruit_STMPE610.h"

// This is calibration data for the raw touch data to the screen coordinates
#define TS_MINX 290
#define TS_MINY 285
#define TS_MAXX 7520
#define TS_MAXY 7510
#define TS_I2C_ADDRESS 0x4d


Adafruit_STMPE610 ts = Adafruit_STMPE610();

// Size of the color selection boxes and the paintbrush size
#define BOXSIZE 40

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

uint8_t tp[5];

static bool eth_connected = false;

void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname("esp32-ethernet1");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}


void setup() {
        // TODO: Power Up UEXT if 32U4

   delay(1000);
   Serial.begin (115200);
   while (!Serial);
   Serial.print ("Demo started");
  
  WiFi.onEvent(WiFiEvent);
  ETH.begin();

  tft.begin();

   Wire.begin();
   pinMode(TFT_DC, OUTPUT);
  // read diagnostics (optional but can help debug problems)
  //uint8_t x = tft.readcommand8(ILI9341_RDMODE);
   delay(1000);

   ts.begin(TS_I2C_ADDRESS);
   timeClient.begin();
}

void loop(void) {

// This is just a draw some data Demo

// Clear Screen
tft.fillScreen(ILI9341_BLACK);
// Set some fancy background
testFastLines(ILI9341_DARKGREY,ILI9341_DARKCYAN);

// Print "current date and time"
//tft.setCursor(5,5);
//tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(2);
//tft.println("29-05-18      11:28"); //TODO: Print the real date and time


// Print "room temperature"
tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(1);
tft.setCursor(5,5);
tft.print("IPv4       : ");
tft.println(ETH.localIP());

tft.setCursor(5,25);
tft.print("Gw         : ");
tft.println(ETH.gatewayIP());

tft.setCursor(5,45);
tft.print("Nameserver : ");
tft.println(ETH.dnsIP());

tft.setCursor(5,65);
tft.print("Network    : ");
tft.println(ETH.networkID());

tft.setCursor(5,85);
tft.print("broadcast  : ");
tft.println(ETH.broadcastIP());

tft.setCursor(5,105);
tft.print("Subnet     : ");
tft.print(ETH.subnetMask());
tft.print(" CIDR: ");
tft.println(ETH.subnetCIDR());

tft.setCursor(5,125);
tft.print("Hostname   : ");
tft.println(ETH.getHostname());

tft.setCursor(5,145);
tft.print("Link       : ");
tft.println(ETH.linkUp());

tft.setCursor(5,165);
tft.print("MAC        : ");
tft.println(ETH.macAddress());

tft.setCursor(5,185);
tft.print("Full Duplex: ");
tft.println(ETH.fullDuplex());
tft.setCursor(5,205);

tft.print("MBs        : ");
tft.println(ETH.linkSpeed());
tft.setCursor(5,225);

timeClient.update();
tft.print("day / time :  ");

int day=(timeClient.getDay());

if (day == 0 ){tft.print("Sunday : "); }
if (day == 1 ){tft.print("Monday : "); }
if (day == 2 ){tft.print("Tuesday : "); }
if (day == 3 ){tft.print("Wednesday : "); }
if (day == 4 ){tft.print("Thursday : "); }
if (day == 5 ){tft.print("Friday : ");   }
if (day == 6 ){tft.print("Saturday : "); }

tft.println(timeClient.getFormattedTime());

//tft.print("current date :  ");
//tft.println(timeClient.geDate());
//tft.print("IPv6       : ");
//tft.println(ETH.localIPv6());






// tft.drawCircle(138, 54, 4, ILI9341_GREEN);
// tft.drawCircle(138, 54, 5, ILI9341_GREEN);
//tft.setCursor(78,85);
//tft.setTextColor(ILI9341_GREEN);  tft.setTextSize(1);
//tft.println("ROM TEMPERATURE");


// Now print Message box with two yes/no buttons
//tft.fillRoundRect(10,120, 220, 190, 8, ILI9341_OLIVE);
//tft.drawRoundRect(10,120, 220, 190, 8, ILI9341_WHITE);

//tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(2);
//tft.fillRoundRect(20,150, 200, 80,8, ILI9341_BLUE);
//tft.setCursor(90, 165);
//tft.println("refresh");
//tft.setCursor(40, 190);
//tft.println(ETH.localIP());
//tft.drawRoundRect(20,150, 200, 80, 8, ILI9341_WHITE);
// Get the choise
bool answer = Get_yes_no();

if (answer == true)
{
  // Some animation while "write to eeprom"
testFilledRects(ILI9341_DARKGREEN,ILI9341_BLUE);
tft.setCursor(80, 150);
tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(2);
tft.println("Working...");
} else {
  tft.fillScreen(ILI9341_BLACK);


tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(1);

tft.setCursor(10, 10);
tft.println("Working...");
//tft.setCursor(10, 20);
bool gateway = Ping.ping(ETH.gatewayIP(),10);
if ( gateway == true ){
  tft.println("ping default gateway  => ok");
  tft.print("         average time => ");
  tft.println(Ping.averageTime());
  }
else{
  tft.println("ping default gateway  => fails");
 }
  tft.println(" ");
//
bool dns = Ping.ping(ETH.dnsIP(),10);
if ( dns == true ){
  tft.println("ping nameserver       => ok");
  tft.print("       average time   => ");
  tft.println(Ping.averageTime());
  }
else{
  tft.println("ping nameserver      => fails");
 }
 tft.println(" ");
bool google = Ping.ping("google.com",10);
if ( google == true ){
  tft.println("ping google.com       => ok");
  tft.print("       average time   => ");
  tft.println(Ping.averageTime());

  }
else{
  tft.println("ping google.com       => fails");
 }


   
 delay(5000);
}


// fill screen red to show negative choise
delay(500);
}


unsigned long testFastLines(uint16_t color1, uint16_t color2) {
  unsigned long start;
  int           x, y, w = tft.width(), h = tft.height();

  tft.fillScreen(ILI9341_BLACK);
  start = micros();
  for(y=0; y<h; y+=5) tft.drawFastHLine(0, y, w, color1);
  for(x=0; x<w; x+=5) tft.drawFastVLine(x, 0, h, color2);

  return micros() - start;
}

unsigned long testFilledRects(uint16_t color1, uint16_t color2) {
  unsigned long start, t = 0;
  int           n, i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  n = min(tft.width(), tft.height());
  for(i=n; i>0; i-=6) {
    i2    = i / 2;
    start = micros();
    tft.fillRect(cx-i2, cy-i2, i, i, color1);
    t    += micros() - start;
    // Outlines are not included in timing results
    tft.drawRect(cx-i2, cy-i2, i, i, color2);
    yield();
  }

  return t;
}

bool Get_yes_no(void){
TS_Point p;
    tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(2);

    tft.fillRoundRect(20,250, 100, 50,8, ILI9341_BLACK);
    tft.setCursor(50, 265);
    tft.println("Ping");
    tft.drawRoundRect(20,250, 100, 50, 8, ILI9341_WHITE);

    tft.fillRoundRect(120,250, 100, 50,8, ILI9341_BLACK);
    tft.setCursor(130, 265);
    tft.println("Refresh");
    tft.drawRoundRect(120,250, 100, 50, 8, ILI9341_WHITE);


while (1){
      delay(50);
    p = ts.getPoint();

    if (p.z != 129){


      p.x = map(p.x, TS_MINX, TS_MAXX, 0, tft.width());
      p.y = map(p.y, TS_MINY, TS_MAXY, 0, tft.height());
      p.y = 320 - p.y;

      //  tft.fillCircle(p.x, p.y, 5, ILI9341_YELLOW);


    if ((p.y > 250) && (p.y<300)){

      if ((p.x> 20) && (p.x < 220))
            if (p.x>120)
            {
              tft.fillRoundRect(120,250, 100, 50,8, ILI9341_OLIVE);
              tft.setCursor(130, 265);
              tft.println("Refresh");
              tft.drawRoundRect(120,250, 100, 50, 8, ILI9341_WHITE);

              delay(100);
              return true;
            }
            else{

              tft.fillRoundRect(20,250, 100, 50,8, ILI9341_OLIVE);
              tft.setCursor(50, 265);
              tft.println("Ping");
              tft.drawRoundRect(20,250, 100, 50, 8, ILI9341_WHITE);

              delay(100);
              return false;
                   }

    }

  }
}
}

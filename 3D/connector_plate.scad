



$fn=50;

translate([5,44,0]) collect_parts();

module collect_parts(){
    difference(){
        base();
        mount_holes();
        translate([59,9,0]) rj45();
        translate([5,9,0]) usb();
        }   
    }
  
  
// usb-b 14x24   
module usb(){
    translate([5,0,-1]) cube([14,15,15]);
    translate([-3,7.5,-1]) cylinder(d=3.5,h=10);
    translate([27,7.5,-1]) cylinder(d=3.5,h=10);
    }
 
    
// rj45 16x18x5   
module rj45(){
    translate([0,0,-1]) cube([18,14,15]);
    }
  
// base plate  
//    % cube([88,38,2]);
module base()  
    minkowski(){
        cube([78,28,1]);
        cylinder(d=10,h=1);
       
        }
    
    
    
module mount_holes(){
   translate([-2,-.5,-1]) cylinder(h=10,d=3.5); 
   translate([80,-1,-1]) cylinder(h=10,d=3.5);
   translate([-2,28+1,-1]) cylinder(h=10,d=3.5); 
   translate([80,29,-1]) cylinder(h=10,d=3.5); 
}
// Marten Vijn 2021

// License BSD  

// tft sreen size
o_inner_l=64;
o_inner_w=46;
//thick=3.4;

// tft base
o_outer_l=72;
o_outer_w=51;
o_outer_h=3.4;

o_print_l=87;
o_print_h=1.2;
o_print_w=o_outer_w;


// choose base or front 
base();
//rotate ([180,0,0]) front();

// reusable print layout or for testing
//print();

module print(){
  union(){
   translate([0,0,-10])cube([o_print_l,o_print_w,o_print_h+10]);
      
   // test object   
   // % translate([78,10,0]) cube([8,8,8]); 
      
   // tft base
   translate([7.5,0,o_print_h-0.1]) cube([o_outer_l,o_outer_w,o_outer_h]);
      
   // tft screen   
   translate([14,2.5,o_print_h-0.1]) cube([o_inner_l,o_inner_w,10]);
      
   // holes 42x78 mm
   translate([4.5,4.5,-5]) cylinder(d=4,h=20,$fn=30);
   translate([4.5,46.5,-5]) cylinder(d=4,h=20,$fn=30);
   translate([4.5+78,4.5,-5]) cylinder(d=4,h=20,$fn=30);
   translate([4.5+78,46.5,-5]) cylinder(d=4,h=20,$fn=30); 
  }
    
  }





module front(){
    difference(){
        cube([o_outer_l+18,o_outer_w+4,6]);
        translate([1,2,0.1]) print();
        }
    }
    
module base(){
    difference(){
       cube([o_outer_l+24,o_outer_w+10,6]); 
       translate([2.5,2.5,3]) cube([o_outer_l+19,o_outer_w+5,6]);
      translate([11,4.5,-0.1]) cube([o_outer_l+3,o_outer_w+1.5,6]);
       translate([4.75,5,-4.2]) print(); 

    }
}
